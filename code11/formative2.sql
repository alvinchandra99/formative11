use staging;
CREATE table Product_ClientA(
id INT auto_increment,
name varchar(255),
description text,
artnumber int,
price int,
stock int,
deleted boolean,
brand_name varchar(255),
primary key(id)
);

CREATE table Product_ClientB(
id INT auto_increment,
nama varchar(255),
deksripsi text,
nomor_artikel int,
harga int,
persediaan_barang int,
dihapus boolean,
nama_merek varchar(255),
primary key(id)
);

use production;

CREATE table product(
id INT auto_increment,
name varchar(255),
description text,
art_number int,
price int,
stock int,
is_deleted boolean,
brand varchar(255),
primary key(id)
);

