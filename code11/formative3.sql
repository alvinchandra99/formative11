use staging;

INSERT INTO Product_ClientA(name,description,artnumber,price,stock,deleted,brand_name)
VALUES
("Samsung S9", "128 Gb / 16GB", 00001, 9000000, 5, false, "Samsung"),
("Samsung S1", "128 Gb / 2 GB", 00002, 2000000, 5, false, "Samsung"),
("Samsung S2", "128 Gb / 2 GB", 00003, 2000000, 3, false, "Samsung"),
("Samsung S3", "128 Gb / 3 GB", 00004, 3000000, 4, false, "Samsung"),
("Samsung S4", "128 Gb / 3 GB", 00005, 4000000, 7, false, "Samsung"),
("Xiaomi Mi 1", "128 Gb / 16GB", 00006, 7000000, 10, false, "Xiaomi"),
("Xiaomi Mi 2", "128 Gb / 16GB", 00007, 9000000, 25, false, "Xiaomi"),
("Xiaomi Mi 3", "128 Gb / 16GB", 00008, 9000000, 5, false, "Xiaomi"),
("Blackberry Gemini", "16 Gb / 1GB", 00009, 9000000, 5, false, "Blackberry"),
("Blackberry Dakota", "32 Gb / 2GB", 00010, 9000000, 5, false, "Blackberry");


INSERT INTO Product_ClientB(nama,deksripsi,nomor_artikel,harga, persediaan_barang,dihapus,nama_merek)
VALUES
("Apple X", "128 Gb / 16GB", 00001, 9000000, 5, false, "Apple"),
("Apple 13", "128 Gb / 2 GB", 00002, 2000000, 5, false, "Apple"),
("Apple 11", "128 Gb / 2 GB", 00003, 2000000, 3, false, "Apple"),
("Apple 12", "128 Gb / 3 GB", 00004, 3000000, 4, false, "Apple"),
("Oppo F1", "128 Gb / 3 GB", 00005, 4000000, 7, false, "Oppo"),
("Oppo F2", "128 Gb / 16GB", 00006, 7000000, 10, false, "Oppo"),
("Asus Zenfone 3", "128 Gb / 16GB", 00007, 9000000, 25, false, "Asus"),
("Asus Zenfone 4", "128 Gb / 16GB", 00008, 9000000, 5, false, "Asus"),
("Asus Zenfone 5", "16 Gb / 1GB", 00009, 9000000, 5, false, "Asus"),
("Oppo F3", "32 Gb / 2GB", 00010, 9000000, 5, false, "Oppo");