use production;

ALTER TABLE product
ADD brandId int NOT NULL;

ALTER TABLE product
DROP COLUMN brand;

CREATE TABLE brand(
id int auto_increment,
name varchar(255),
primary key(id)
);