use production;

INSERT into production.brand(name)
SELECT brand_name from staging.product_clienta
UNION
SELECT nama_merek from staging.product_clientb;

ALTER TABLE product 
modify brandId INT NULL; 

ALTER TABLE staging.product_clienta
ADD FOREIGN KEY(brand_name) references production.brand(name);

ALTER TABLE staging.product_clientb
ADD FOREIGN KEY(nama_merek) references production.brand(name);
 
DELETE from product;

INSERT INTO production.product(name, description, art_number, price, stock, is_deleted, brandId)
SELECT brand.id FROM staging.product_clienta 
JOIN production.brand
ON product_clienta.brand_name = brand.name;

INSERT INTO production.product(name, description, art_number, price, stock, is_deleted, brandId)
SELECT brand.id FROM staging.product_clientb 
JOIN production.brand
ON product_clientb.brand_name = brand.name;




